package com.dak.koperasi.Services

import com.dak.koperasi.Utilitites.SupportInterceptor
import com.dak.koperasi.Utilitites.URL_SERVICE
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitServiceKotlin{

    companion object {
        fun getRetrofit(): Retrofit {
            val okHttpClient = OkHttpClient.Builder().apply {
                readTimeout(20, TimeUnit.SECONDS)
                connectTimeout(20, TimeUnit.SECONDS)
                addInterceptor(SupportInterceptor())
            }

            val client = okHttpClient.build()
            val retrofit = Retrofit.Builder()
                .baseUrl(URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit
        }
    }
}