package com.dak.koperasi.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dak.koperasi.Activities.KeranjangActivity
import com.dak.koperasi.Model.Produk
import com.dak.koperasi.R
import com.dak.koperasi.Utilitites.toRupiah
import kotlinx.android.synthetic.main.row_keranjang.view.*

class AdapterListKeranjang(var context: Context, private var list: List<Produk>, var listener: AdapterListKeranjang.MenuInteractionListener): RecyclerView.Adapter<AdapterListKeranjang.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterListKeranjang.ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_keranjang,p0,false))
    }

    override fun getItemCount(): Int=list.size

    override fun onBindViewHolder(p0: AdapterListKeranjang.ViewHolder, p1: Int) {
        p0.bindView(list[p1],p1)
     }

    inner class ViewHolder (v:View):RecyclerView.ViewHolder(v){
        var namaProduk = v.tvNamaItem
        var qty = v.tvQty
        var varianProduk = v.tvVarianItem
        var hargaProduk = v.tvHargaItem
        var btnHapus = v.btnHapus
        fun bindView(produk : Produk,pos:Int){
            namaProduk.text=produk.mNama
            varianProduk.text = produk.mMerk
            hargaProduk.text= toRupiah(produk.mHarga)
            qty.text=produk.mQty
            btnHapus.setOnClickListener{
                listener.onButtonClick(pos)
            }
            //Glide.with(context).load(produk.mImg).into(imgProduk)
        }
    }
    interface MenuInteractionListener{
        fun onButtonClick(menu : Int)
    }
}