package com.dak.koperasi.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dak.koperasi.Model.Simulasi
import com.dak.koperasi.R
import kotlinx.android.synthetic.main.row_keranjang.view.*
import kotlinx.android.synthetic.main.row_simulasi.view.*

class AdapterListSimulasi(var context: Context, private  var list : List<Simulasi>): RecyclerView.Adapter<AdapterListSimulasi.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterListSimulasi.ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_simulasi,p0,false))
    }

    override fun getItemCount(): Int=list.size

    override fun onBindViewHolder(p0: AdapterListSimulasi.ViewHolder, p1: Int) {
        p0.bindView(list[p1],p1)
    }

    inner class ViewHolder (v: View):RecyclerView.ViewHolder(v){
        var tvAngsuran = v.tvAngsuran
        var tvTotal = v.tvTotal
        var tvPoko = v.tvPoko
        var tvBunga = v.tvBunga
        var tvSisaAngsuran =v.tvSisaAngsuran
        fun bindView(produk : Simulasi,pos:Int){
            tvAngsuran.text= produk.angsuran.toString()
            tvTotal.text = produk.total.toString()
            tvPoko.text = produk.pokok.toString()
            tvBunga.text = produk.bunga.toString()
            tvSisaAngsuran.text = produk.sisa_angsuran.toString()
            //Glide.with(context).load(produk.mImg).into(imgProduk)
        }
    }
}