package com.dak.koperasi.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dak.koperasi.Model.MenuPinjaman
import com.dak.koperasi.R
import kotlinx.android.synthetic.main.row_menu_pinjaman.view.*

class AdapterMenuPinjaman(private val context : Context, private var list : List<MenuPinjaman>, var listener : MenuInteractionListener) :
    RecyclerView.Adapter<AdapterMenuPinjaman.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_menu_pinjaman,p0,false))
    }

    override fun getItemCount(): Int=list.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindView(list[p1],p1)
    }

    inner class ViewHolder(v:View) : RecyclerView.ViewHolder(v){
        private var txtTitle = v.txtNamaProduk
        private var txtDesc = v.txtHargaProduk
        private var imgMenu = v.imgProduk
        private var btnAjukan = v.btnAjukan
        private var btnSimulasi = v.btnSimulasi

        fun bindView(menu : MenuPinjaman,pos:Int){
            txtTitle.text=menu.mTitle
            txtDesc.text=menu.mDesc
            imgMenu.setImageResource(menu.mImg)
            btnAjukan.setOnClickListener{
                listener.onAjukanPinjamanClicked(pos)
            }
            btnSimulasi.setOnClickListener{
                listener.onSimulasiClicked(pos)
            }

        }

    }

    interface MenuInteractionListener{
        fun onAjukanPinjamanClicked(menu : Int)
        fun onSimulasiClicked(menu : Int)
    }
}