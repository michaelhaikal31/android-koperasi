package com.dak.koperasi.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.dak.koperasi.Model.Produk
import com.dak.koperasi.R
import com.dak.koperasi.Utilitites.toRupiah
import kotlinx.android.synthetic.main.row_produk.view.*

class AdapterListProduk(var context : Context, private var list : List<Produk>, var listener : AdapterListProduk.MenuInteractionListener):RecyclerView.Adapter<AdapterListProduk.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterListProduk.ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_produk,p0,false))
    }

    override fun getItemCount(): Int=list.size

    override fun onBindViewHolder(p0: AdapterListProduk.ViewHolder, p1: Int) {
        p0.bindView(list[p1],p1)
    }

    inner class ViewHolder(v: View):RecyclerView.ViewHolder(v){

        var namaProduk  = v.txtNamaProduk
        var hargaProduk = v.txtHargaProduk
        var qty = v.txtQty
        var imgProduk = v.imgProduk
        var btnAddKeranjang = v.btnAddKeranjang

        fun bindView(produk : Produk,pos:Int){
            namaProduk.text=produk.mNama
            hargaProduk.text= toRupiah(produk.mHarga)
            qty.text="stok : "+produk.mQty
            btnAddKeranjang.setOnClickListener{
                listener.onButtonClick(pos)
            }
            //Glide.with(context).load(produk.mImg).into(imgProduk)
        }



    }
    interface MenuInteractionListener{
        fun onButtonClick(menu : Int)
    }

}