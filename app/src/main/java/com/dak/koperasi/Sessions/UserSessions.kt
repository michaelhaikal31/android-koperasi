package com.dak.koperasi.Sessions

import android.content.Context
import android.content.SharedPreferences

fun setUserName(context: Context,name : String){
    val editor : SharedPreferences.Editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit()
    editor.putString("user_name",name)
    editor.commit()
}

fun getUserName(context: Context):String{
    val prefs : SharedPreferences = context.getSharedPreferences("userSession",Context.MODE_PRIVATE)
    return prefs.getString("user_name","")
}

fun setMsisdn(context: Context,msisdn : String){
    val editor : SharedPreferences.Editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit()
    editor.putString("msisdn",msisdn)
    editor.commit()
}

fun getMsisdn(context: Context):String{
    val prefs : SharedPreferences = context.getSharedPreferences("userSession",Context.MODE_PRIVATE)
    return prefs.getString("msisdn","")
}

fun setLoginState(context: Context,hasLogin : Boolean){
    val editor : SharedPreferences.Editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit()
    editor.putBoolean("login_state",hasLogin);
    editor.commit()
}

fun getLoginState(context: Context):Boolean{
    val prefs : SharedPreferences = context.getSharedPreferences("userSession",Context.MODE_PRIVATE)
    return prefs.getBoolean("login_state",false)
}

fun setSaldoUser(context: Context,saldo:String){
    val editor : SharedPreferences.Editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit()
    editor.putString("saldo",saldo);
    editor.commit()
}

fun getSaldoUser(context: Context):String{
    val prefs : SharedPreferences = context.getSharedPreferences("userSession",Context.MODE_PRIVATE);
    return prefs.getString("saldo","")
}