package com.dak.koperasi.Utilitites

import okhttp3.Interceptor
import okhttp3.Response

class SupportInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request?.newBuilder()
                ?.addHeader("Content-Type", "multipart/form-data")
                //?.addHeader("x-api-key", Constant.getKey(24))
                ?.build()
        return chain.proceed(request)
    }
}