package com.dak.koperasi.Utilitites

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import com.dak.koperasi.R

var dialog : AlertDialog? = null

fun showSimpleDialogMessage(context: Context,message : String){
    val alert : AlertDialog.Builder = AlertDialog.Builder(context)
    alert.setMessage(message)
    alert.setPositiveButton("Ok", DialogInterface.OnClickListener(
        return
    ))
    alert.show()
}



fun showListenerDialogMessage(context: Context,message : String,listener: DialogListener){
    val alert : AlertDialog.Builder = AlertDialog.Builder(context)
    alert.setMessage(message)
    alert.setPositiveButton("Ok"){
     dialog,which->
        listener.onPositiveDialogBtnClickListener()
    }
    alert.setNegativeButton("Cancel"){
        dialog,which->
        listener.onNegativeDialogBtnClickListener()
    }
    alert.show()
}

fun createProgressDialog(context: Context,inflater:LayoutInflater){
    val builder = AlertDialog.Builder(context)
    val view = inflater.inflate(R.layout.dialog_progress_bar, null)
    builder.setView(view)
    builder.create()
    dialog = builder.create()
}

fun showProgressDialog(){
    dialog!!.show()
}
fun dismissProgressDialog(){
    dialog!!.dismiss()
}



interface DialogListener{
    fun onPositiveDialogBtnClickListener()
    fun onNegativeDialogBtnClickListener()
}


