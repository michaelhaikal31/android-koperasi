package com.dak.koperasi.Utilitites

import java.lang.Exception
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

fun toRupiah(nominal : String):String{
        var result:String
        try {
                val localeID =  Locale("in", "ID")
                val numberFormat = NumberFormat.getCurrencyInstance(localeID)
                return numberFormat.format(nominal.toDouble()).toString().replace("Rp","Rp.")
        }catch (e:Exception){
                result="0"
                println("asd exception[toRupiah] ${e.message}")
        }
        return result
}