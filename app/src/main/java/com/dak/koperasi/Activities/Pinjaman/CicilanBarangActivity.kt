package com.dak.koperasi.Activities.Pinjaman

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.Space
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.dak.koperasi.Activities.KeranjangActivity
import com.dak.koperasi.Activities.SplashScreenActivity
import com.dak.koperasi.Adapters.AdapterListKeranjang
import com.dak.koperasi.Adapters.AdapterListProduk
import com.dak.koperasi.Interfaces.ApiServiceKotlin
import com.dak.koperasi.Model.Produk
import com.dak.koperasi.R
import com.dak.koperasi.Services.RetrofitServiceKotlin
import com.dak.koperasi.Sessions.getMsisdn
import com.dak.koperasi.Utilitites.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class CicilanBarangActivity : AppCompatActivity(), AdapterListProduk.MenuInteractionListener{

    override fun onButtonClick(menu: Int) {
//       showListenerDialogMessage(this,"Barang berhasil dipilih,\n ajukan sekarang?",Dialog )
        showListenerDialogMessage(this, "Barang berhasil dipilih, \n ajukan sekarang?", object : DialogListener {
            override fun onPositiveDialogBtnClickListener() {
                startActivity(Intent(this@CicilanBarangActivity, KeranjangActivity::class.java))
                finish()
                return
            }

            override fun onNegativeDialogBtnClickListener() {}
        })
    }

    private lateinit var recyclerView : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cicilan_barang)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("Cicilan Barang")
        recyclerView = findViewById(R.id.rvCicilanBarang)
        val gridLayoutManager = GridLayoutManager(this,2,GridLayoutManager.VERTICAL, false)
        recyclerView.layoutManager=gridLayoutManager
        recyclerView.addItemDecoration(Space(2, 10, true, 0))

        getListProduk()
    }

    private fun getListProduk() {
        createProgressDialog(this,layoutInflater)
        showProgressDialog()
        val jsonParam = JSONObject()
        try{
           jsonParam.put("notelp", "089536458572"/*getMsisdn(this)*/)
            jsonParam.put("kategori","Barang")
        }catch (e:Exception){
            println("asd exception create param [MainActiity] "+e.message)
            showListenerDialogMessage(this,"Oops, something wrong with our system",object: DialogListener {
                override fun onPositiveDialogBtnClickListener() { return }
                override fun onNegativeDialogBtnClickListener() {}
            })
        }
        println("asd jsonParam[CicilanBarang] $jsonParam")
        val apiInterface = RetrofitServiceKotlin.getRetrofit().create(ApiServiceKotlin::class.java)
        val call = apiInterface.getLayanan(jsonParam.toString())
        call.enqueue(object:Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                println("asd failure ${t.message}")
                showSimpleDialogMessage(this@CicilanBarangActivity, DESC_CANNOT_CONNECT)
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try{
                    val jsonResponse = JSONObject(response.body())
                    println("asd response ${response.body()}")
                    when(jsonResponse.getString("rc")){
                        "00"->{
                            val size = jsonResponse.getJSONArray("data").length()
                            val list = ArrayList<Produk>()
                            var jsonProduk = JSONObject()
                            for(i in 0 until size){
                                jsonProduk = jsonResponse.getJSONArray("data").getJSONObject(i)
                                list.add(Produk(jsonProduk.getInt("id_produk"),
                                    jsonProduk.getString("nama_produk"),
                                    jsonProduk.getString("merk"),
                                    jsonProduk.getString("harga"),
                                    jsonProduk.getString("jumlah_produk"),
                                    jsonProduk.getString("foto_produk")
                                    ))
                            }
                            recyclerView.adapter = AdapterListProduk(this@CicilanBarangActivity,list,this@CicilanBarangActivity)
                            dismissProgressDialog()
                        }
                        "20"->{
                            dismissProgressDialog()
                            showSimpleDialogMessage(this@CicilanBarangActivity,jsonResponse.getString("rm"))
                        }
                        "IE"->{
                            dismissProgressDialog()
                            showSimpleDialogMessage(this@CicilanBarangActivity,jsonResponse.getString("rm"))
                        }
                        else ->{
                            dismissProgressDialog()
                            println("asd response[cicilan barang] ${jsonResponse.getString("rm")}")
                        }
                    }
                }catch (e:Exception){
                    println("asd exception ${e.message}")
                    showSimpleDialogMessage(this@CicilanBarangActivity, DESC_SERVER_RESPONSE_FAILED)
                    dismissProgressDialog()
                }
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.list_produk,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if(id==R.id.action_keranjang){
            startActivity(Intent(this@CicilanBarangActivity, KeranjangActivity::class.java))
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }



}
