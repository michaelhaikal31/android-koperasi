package com.dak.koperasi.Activities.Listrik

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import com.dak.koperasi.Activities.CheckoutActivity
import com.dak.koperasi.R
import kotlinx.android.synthetic.main.activity_token_listrik.*

class TokenListrikActivity : AppCompatActivity(), View.OnClickListener {
    private var mLastCLick : Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_token_listrik)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnTokenListrik.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
         if(SystemClock.elapsedRealtime() - mLastCLick<1000){
            return
        }
        mLastCLick = SystemClock.elapsedRealtime()
        handleOnClick(v)

    }

    private fun handleOnClick(v:View?) {
        when(v?.id){
            R.id.btnTokenListrik -> {
                startActivity(Intent(this, CheckoutActivity::class.java))
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
