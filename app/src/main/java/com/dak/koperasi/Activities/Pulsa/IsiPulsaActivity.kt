package com.dak.koperasi.Activities.Pulsa

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.*
import com.dak.koperasi.Activities.CheckoutActivity
import com.dak.koperasi.R
import kotlinx.android.synthetic.main.activity_isi_pulsa.*

class IsiPulsaActivity : AppCompatActivity(),View.OnClickListener {

    private val MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1122
    private var mLastCLick : Long = 0
    private var listView : ListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_isi_pulsa)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listView = findViewById(R.id.lvIsiPulsa)
        btnContacts.setOnClickListener(this)
        btnIsiPulsa.setOnClickListener(this)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val contactData = data?.getData()
            val c = this.getContentResolver().query(contactData!!, null, null, null, null)
            if (c!!.moveToFirst()) {

                var phoneNumber = ""
                var emailAddress = ""
                val name = c!!.getString(c!!.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val contactId = c!!.getString(c!!.getColumnIndex(ContactsContract.Contacts._ID))
                //http://stackoverflow.com/questions/866769/how-to-call-android-contacts-list   our upvoted answer

                var hasPhone = c!!.getString(c!!.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                if (hasPhone.equals("1", ignoreCase = true))
                    hasPhone = "true"
                else
                    hasPhone = "false"

                if (java.lang.Boolean.parseBoolean(hasPhone)) {
                    val phones = this.getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                        null,
                        null
                    )
                    while (phones!!.moveToNext()) {
                        phoneNumber =
                            phones!!.getString(phones!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    }
                    phones!!.close()
                }

                // Find Email Addresses
                val emails = this.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,
                    null,
                    null
                )
                while (emails!!.moveToNext()) {
                    emailAddress =
                        emails!!.getString(emails!!.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                }
                emails!!.close()

                //mainActivity.onBackPressed();
                // Toast.makeText(mainactivity, "go go go", Toast.LENGTH_SHORT).show();


                //tvphone.setText("Phone: "+phoneNumber);
                //tvmail.setText("Email: "+emailAddress);
                txtIsiPulsa.setText(phoneNumber)

                Log.d("curs", name + " num" + phoneNumber + " " + "mail" + emailAddress)
            }
            c!!.close()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
      if(SystemClock.elapsedRealtime()-mLastCLick<1000){
          return
      }
        mLastCLick = SystemClock.elapsedRealtime()
        handleOnClick(v)
    }

    private fun handleOnClick(v : View?) {
        when (v?.id) {
            R.id.btnIsiPulsa -> {
                startActivity(Intent(this, CheckoutActivity::class.java))
            }
            R.id.btnContacts -> {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_CONTACTS
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                    // Permission is not granted
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.READ_CONTACTS
                        )
                    ) {
                        val alert = AlertDialog.Builder(this)
                        alert.setTitle("Permission Request")
                        alert.setMessage("Dengan mengizinkan kami mengakses kontak anda akan memudahkan kami untuk memberikan fitur pencarian nomor telepon yang lebih baik")
                        alert.setPositiveButton("Ok") { dialog, which ->
                            ActivityCompat.requestPermissions(
                                this,
                                arrayOf(Manifest.permission.READ_CONTACTS),
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS
                            )
                        }
                        alert.setNegativeButton(
                            "Batal",
                            DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
                        alert.create()
                        alert.show()

                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.READ_CONTACTS),
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    // Permission has already been granted
                    val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                    startActivityForResult(intent, 1)
                }
            }
        }
    }

    private fun loadList() {
        var list = ArrayList<String>()
        list.add("10.000")
        list.add("20.000")
        list.add("30.000")
        list.add("50.000")
        list.add("100.000")
        list.add("200.000")
        var adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, list)
        listView?.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView?.adapter = adapter
        listView?.setOnItemClickListener({ parent, view, position, id ->
        })

    }



}

