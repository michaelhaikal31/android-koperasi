package com.dak.koperasi.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.os.Handler
import com.dak.koperasi.Interfaces.ApiServiceKotlin
import com.dak.koperasi.R
import com.dak.koperasi.Services.RetrofitServiceKotlin
import com.dak.koperasi.Sessions.setSaldoUser
import com.dak.koperasi.Utilitites.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception


class SplashScreenActivity : AppCompatActivity() {
    val SPLASH_DISPLAY_LENGTH : Long = 1000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        Handler().postDelayed(Runnable {
            val mainIntent = Intent(this@SplashScreenActivity, MainActivity::class.java)
            startActivity(mainIntent)
            finish()
        }, SPLASH_DISPLAY_LENGTH)
    }

    fun initialize(){
        createProgressDialog(this,layoutInflater)
        showProgressDialog()
        val jsonParam = JSONObject()
        try{
            jsonParam.put("msisdn",txtUserName.text)
        }catch (e: Exception){
            showSimpleDialogMessage(this,"Oops, terjadi kesalahan")
            println("asd exception [LoginActivity]" +e.message)
            return
        }
        val apiInterface = RetrofitServiceKotlin.getRetrofit().create(ApiServiceKotlin::class.java)
        val call = apiInterface.login(jsonParam.toString())
        call.enqueue(object: retrofit2.Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                dismissProgressDialog()
                showSimpleDialogMessage(this@SplashScreenActivity, TITLE_CANNOT_CONNECT)
                println("asd failure [login] "+t.message)
                btnLogin?.isEnabled = true
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    //System.out.println("asd respon "+tripleDes.decrypt(response.toString()));
                    //val jsonObject = JSONObject(response.body())
                    val jsonObject = JSONObject(jsonResponse)
                    val data  = jsonObject.getJSONObject("data")
                    if (jsonObject.getString("rc") == "00") {
                        setSaldoUser(this@SplashScreenActivity,data.getString("saldo"))
                        startActivity(Intent(this@SplashScreenActivity,SplashScreenActivity::class.java))
                        finish()
                    } else {
                        showSimpleDialogMessage(this@SplashScreenActivity,data.getString("rm"))
                    }
                } catch (e: Exception) {
                    showSimpleDialogMessage(this@SplashScreenActivity, DESC_SERVER_RESPONSE_FAILED)
                    println("asd [login] catch " + e.toString())
                }
                dismissProgressDialog()
                btnLogin?.setEnabled(true)
            }
        })
    }

    private val jsonResponse = "{\n" +
            "\"rc\":200,\n" +
            "\"rm\":\"Success\",\n" +
            "\"data\":{\n" +
            " \"notelp\":\"089536458572\",\n" +
            " \"username\":\"Rolem ipsum\",\n" +
            " \"saldo\":\"120.000\"\n" +
            "}\n" +
            "}";
}
