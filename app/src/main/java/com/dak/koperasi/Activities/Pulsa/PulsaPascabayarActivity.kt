package com.dak.koperasi.Activities.Pulsa

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import com.dak.koperasi.Activities.CheckoutActivity
import com.dak.koperasi.R
import kotlinx.android.synthetic.main.activity_pulsa_pascabayar.*

class PulsaPascabayarActivity : AppCompatActivity(), View.OnClickListener {

    private var mLastClick: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pulsa_pascabayar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnProsesPulsaPascaBayar.setOnClickListener {this }
    }

    override fun onClick(v: View?) {
      if(SystemClock.elapsedRealtime() - mLastClick<100){
          return
      }
        mLastClick = SystemClock.elapsedRealtime()

handleOnClick(v)
    }

    private fun handleOnClick( v:View?) {
        when(v?.id){
            R.id.btnProsesPulsaPascaBayar -> {
                startActivity(Intent(this, CheckoutActivity::class.java))}
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
