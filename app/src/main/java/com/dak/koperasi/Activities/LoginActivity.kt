package com.dak.koperasi.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import com.bumptech.glide.load.engine.Resource
import com.dak.koperasi.Adapters.SlidingImageAdapter
import com.dak.koperasi.Interfaces.ApiServiceKotlin
import com.dak.koperasi.R
import com.dak.koperasi.Services.RetrofitServiceKotlin
import com.dak.koperasi.Sessions.setLoginState
import com.dak.koperasi.Sessions.setMsisdn
import com.dak.koperasi.Sessions.setSaldoUser
import com.dak.koperasi.Sessions.setUserName
import com.dak.koperasi.Utilitites.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception
import javax.security.auth.callback.Callback

class LoginActivity : AppCompatActivity() {

    var mPager : ViewPager? = null
    var tabDots : TabLayout?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener(View.OnClickListener {
            validasiField()
        })
        mPager = findViewById(R.id.pager)
        tabDots = findViewById(R.id.tabDots)
        tabDots?.setupWithViewPager(mPager, true)
        loadSlider()

    }

    private fun loadSlider(){
        val list  = ArrayList<Int>()
        list.add(resources.getIdentifier("slide_login_1","drawable",this.packageName))
        list.add(resources.getIdentifier("slide_login_2","drawable",this.packageName))
        list.add(resources.getIdentifier("slide_login_3","drawable",this.packageName))
        mPager?.setAdapter(SlidingImageAdapter(this, list))
    }



    private fun validasiField() {
        if(txtUserName.text.length>0 && txtPassword.text.length>0){
            login()
        }else{
            showSimpleDialogMessage(this,"Silahkan lengkapi data login anda !")
        }
    }

    private fun login() {
        createProgressDialog(this,layoutInflater)
        showProgressDialog()
        val jsonParam = JSONObject()
        try{
            jsonParam.put("notelp",txtUserName.text)
            jsonParam.put("password",txtPassword.text)
        }catch (e:Exception){
            showSimpleDialogMessage(this,"Oops, terjadi kesalahan")
            println("asd exception [LoginActivity]" +e.message)
            return
        }
        val apiInterface = RetrofitServiceKotlin.getRetrofit().create(ApiServiceKotlin::class.java)
        val call = apiInterface.login(jsonParam.toString())
        call.enqueue(object: retrofit2.Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                dismissProgressDialog()
                showSimpleDialogMessage(this@LoginActivity,TITLE_CANNOT_CONNECT)
                println("asd failure [login] "+t.message)
                btnLogin?.isEnabled = true
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    println("asd response [Login] "+response.body().toString())
                    val jsonObject = JSONObject(response.body())
                    val data  = jsonObject.getJSONObject("data")
                    if (jsonObject.getString("rc") == "200") {
                        setLoginState(this@LoginActivity,true)
                        setUserName(this@LoginActivity,data.getString("username"))
                        setMsisdn(this@LoginActivity,data.getString("username"))
                        setSaldoUser(this@LoginActivity,data.getString("saldo"))
                        startActivity(Intent(this@LoginActivity,SplashScreenActivity::class.java))
                        finish()
                    } else {
                        showSimpleDialogMessage(this@LoginActivity,data.getString("rm"))
                    }
                } catch (e: Exception) {
                    showSimpleDialogMessage(this@LoginActivity,DESC_SERVER_RESPONSE_FAILED)
                    println("asd [login] catch " + e.toString())
                }

                dismissProgressDialog()
                btnLogin?.setEnabled(true)
            }
        })
    }
    private val jsonResponse = "{\n" +
            "\"rc\":200,\n" +
            "\"rm\":\"Success\",\n" +
            "\"data\":{\n" +
            " \"notelp\":\"089536458572\",\n" +
            " \"username\":\"Rolem ipsum\",\n" +
            " \"saldo\":\"120.000\"\n" +
            "}\n" +
            "}";
}
