package com.dak.koperasi.Activities

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.Toast
import com.dak.koperasi.R
import java.text.SimpleDateFormat
import java.util.*

class MutasiActivity : AppCompatActivity() {
    
    private var mLastClick : Long = 0
    private var btnStartDate : Button?=null
    private var btnEndDate : Button?=null
    internal var myStartCalendar = Calendar.getInstance()
    internal var myEndCalendar = Calendar.getInstance()
    private var id : Int?=null
    private var startDate : Boolean?=false
    private var endDate : Boolean?=false
    private var date : DatePickerDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mutasi)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btnStartDate = findViewById(R.id.btnStartDate)
        btnEndDate = findViewById(R.id.btnEndDate)


        val date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            if (id == 1) {
                myStartCalendar.set(Calendar.YEAR, year)
                myStartCalendar.set(Calendar.MONTH, monthOfYear)
                myStartCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel()
            } else {
                myEndCalendar.set(Calendar.YEAR, year)
                myEndCalendar.set(Calendar.MONTH, monthOfYear)
                myEndCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel()
            }
        }

        btnStartDate?.setOnClickListener(View.OnClickListener {
            id = 1
            DatePickerDialog(
                this@MutasiActivity, date, myStartCalendar
                    .get(Calendar.YEAR), myStartCalendar.get(Calendar.MONTH),
                myStartCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
            startDate = true
        })
        btnEndDate?.setOnClickListener(View.OnClickListener {
            id = 2
            DatePickerDialog(
                this@MutasiActivity, date, myEndCalendar
                    .get(Calendar.YEAR), myEndCalendar.get(Calendar.MONTH),
                myEndCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
            endDate = true
        })

    }

    private fun updateLabel() {
        val myFormat = "dd-MM-yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        if (id == 1) {
            btnStartDate?.setText(sdf.format(myStartCalendar.time))
        } else {
            btnEndDate?.setText(sdf.format(myEndCalendar.time))
        }
        if (startDate!! && endDate!!) {
            Toast.makeText(this,"get list",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
