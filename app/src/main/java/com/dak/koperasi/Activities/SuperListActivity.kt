package com.dak.koperasi.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.dak.koperasi.R
import kotlinx.android.synthetic.main.activity_super_list.*

class SuperListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_super_list)
        loadList()
    }
    private fun loadList() {
        var list = ArrayList<String>()
        list.add("10.000")
        list.add("20.000")
        list.add("30.000")
        list.add("50.000")
        list.add("100.000")
        list.add("200.000")
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_single_choice, list)
        lvSuper?.adapter = adapter
//        listView?.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
//            Toast.makeText(this,"id : "+position,Toast.LENGTH_SHORT).show()
//        }
        lvSuper?.setItemChecked(2,true)
    }
}
