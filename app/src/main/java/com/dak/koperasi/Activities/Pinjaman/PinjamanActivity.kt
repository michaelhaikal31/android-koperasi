package com.dak.koperasi.Activities.Pinjaman

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.dak.koperasi.Adapters.AdapterMenuPinjaman
import com.dak.koperasi.Model.MenuPinjaman
import com.dak.koperasi.R


class PinjamanActivity : AppCompatActivity(),AdapterMenuPinjaman.MenuInteractionListener{

    private lateinit var recylerView : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pinjaman)
//        val toolbar: Toolbar = findViewById(R.id.custom_toolbar)
//        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recylerView = findViewById(R.id.rvPinjaman)
        loadMenu()
    }

    private fun loadMenu() {
        var list = ArrayList<MenuPinjaman>()
        list.add(MenuPinjaman("Pinjaman Dana","Lakukan pinjaman dana dengan mudah bersama kami",R.drawable.ic_pinjaman_dana))
        list.add(MenuPinjaman("Cicilan Barang","Banyak penawaran khusus dari kami hanya untuk anda",R.drawable.ic_cicilan_barang))
        list.add(MenuPinjaman("Cicilan Motor","Ayo wujudkan motor impian anda bersama kami dalam program cicilan motor",R.drawable.ic_cicilan_motor))
        var layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recylerView.adapter=AdapterMenuPinjaman(this,list,this)
        recylerView.layoutManager=layoutManager

    }

    override fun onAjukanPinjamanClicked(menu: Int) {
        when(menu){
            0->Toast.makeText(this,"ajukan menu1",Toast.LENGTH_SHORT).show()
            1->startActivity(Intent(this,CicilanBarangActivity::class.java))
            2->Toast.makeText(this,"ajukan menu3",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSimulasiClicked(menu: Int) {
        when(menu){
            0->Toast.makeText(this,"simulasi menu1",Toast.LENGTH_SHORT).show()
            1->Toast.makeText(this,"simulasi menu2",Toast.LENGTH_SHORT).show()
            2->Toast.makeText(this,"simulasi menu3",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
