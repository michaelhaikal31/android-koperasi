package com.dak.koperasi.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.dak.koperasi.Adapters.AdapterListKeranjang
import com.dak.koperasi.Adapters.AdapterListSimulasi
import com.dak.koperasi.Interfaces.ApiServiceKotlin
import com.dak.koperasi.Model.Produk
import com.dak.koperasi.Model.Simulasi
import com.dak.koperasi.R
import com.dak.koperasi.Services.RetrofitServiceKotlin
import com.dak.koperasi.Utilitites.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KeranjangActivity : AppCompatActivity(), AdapterListKeranjang.MenuInteractionListener {


    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerView2: RecyclerView

    private lateinit var listView :myWidget
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keranjang)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setTitle("Keranjang")
        recyclerView = findViewById(R.id.rvKeranjang)
        recyclerView2 = findViewById(R.id.rvSimulasi)
        listView = findViewById(R.id.listViewPulsa)
        recyclerView2.layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        getAllKeranjang()

    }

    private fun getAllKeranjang() {
        createProgressDialog(this, layoutInflater)
        showProgressDialog()
        val jsonParam = JSONObject()
        try {
            jsonParam.put("notelp", "089536458572"/*getMsisdn(this)*/)
        } catch (e: Exception) {
            showListenerDialogMessage(this, "Oops, something wrong with our system", object : DialogListener {
                override fun onPositiveDialogBtnClickListener() {
                    return
                }

                override fun onNegativeDialogBtnClickListener() {}
            })
        }
        val apiInterface = RetrofitServiceKotlin.getRetrofit().create(ApiServiceKotlin::class.java)
        val call = apiInterface.getAllKeranjang(jsonParam.toString())
        call.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                showSimpleDialogMessage(this@KeranjangActivity, DESC_CANNOT_CONNECT)
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    val jsonResponse = JSONObject(response.body())
                    println("asd response getAllkeranjang ${response.body()}")
                    when (jsonResponse.getString("rc")) {
                        "00" -> {
                            val list = ArrayList<Produk>()
                            var jsonData = jsonResponse.getJSONObject("data")
                            var jsonProduk =  jsonData.getJSONObject("produk")

                                list.add(
                                    Produk(
                                        1,
                                        jsonProduk.getString("nama_produk"),
                                        jsonProduk.getString("type"),
                                        jsonProduk.getString("harga"),
                                        jsonProduk.getString("jumlah"),
                                        jsonProduk.getString("foto_produk")
                                    )
                                )
                                println("asd show list "+list.size)
                                recyclerView.adapter = AdapterListKeranjang(this@KeranjangActivity, list, this@KeranjangActivity)

                            var jsonSimulasiLength = jsonData.getJSONArray("simulasi").length()
                            var listSimulasi =ArrayList<Simulasi>()
                            var jsonSimulasi = JSONObject()
                            for(i in 0 until jsonSimulasiLength){
                                jsonSimulasi = jsonData.getJSONArray("simulasi").getJSONObject(i)
                                listSimulasi.add(Simulasi(
                                    jsonSimulasi.getInt("total"),
                                    jsonSimulasi.getInt("sisa_angsuran"),
                                    jsonSimulasi.getInt("pokok"),
                                    jsonSimulasi.getString("angsuran"),
                                    jsonSimulasi.getInt("bunga")))
                            }
                            println("asd show list Simulasi "+listSimulasi.size)
                            recyclerView2.adapter = AdapterListSimulasi(this@KeranjangActivity, listSimulasi)


                            dismissProgressDialog()

                        }
                        "20" -> {
                            dismissProgressDialog()
                            showSimpleDialogMessage(this@KeranjangActivity, jsonResponse.getString("rm"))
                        }
                        "IE" -> {
                            dismissProgressDialog()
                            showSimpleDialogMessage(this@KeranjangActivity, jsonResponse.getString("rm"))
                        }
                        else -> {
                            dismissProgressDialog()
                            println("asd response[cicilan barang] ${jsonResponse.getString("rm")}")
                        }
                    }
                } catch (e: Exception) {
                    println("asd failed ${e.toString()}")
                }
                dismissProgressDialog()
            }

        })
    }
    override fun onButtonClick(menu: Int) {
        showListenerDialogMessage(this, "Apakah anda yakin \n menghapus item ini?", object : DialogListener {
            override fun onPositiveDialogBtnClickListener() {
                var listSimulasi =ArrayList<Produk>()
                listSimulasi.drop(menu)
                recyclerView.adapter = AdapterListKeranjang(this@KeranjangActivity, listSimulasi, this@KeranjangActivity)
                return
            }

            override fun onNegativeDialogBtnClickListener() {}
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
