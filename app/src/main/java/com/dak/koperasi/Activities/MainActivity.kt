package com.dak.koperasi.Activities

import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.support.v4.widget.DrawerLayout
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.dak.koperasi.Activities.Listrik.TokenListrikActivity
import com.dak.koperasi.Activities.Pinjaman.PinjamanActivity
import com.dak.koperasi.Activities.Pulsa.PulsaPascabayarActivity
import com.dak.koperasi.Interfaces.ApiServiceKotlin
import com.dak.koperasi.R
import com.dak.koperasi.Services.RetrofitServiceKotlin
import com.dak.koperasi.Sessions.getMsisdn
import com.dak.koperasi.Sessions.getSaldoUser
import com.dak.koperasi.Sessions.setSaldoUser
import com.dak.koperasi.Utilitites.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {


    var menu1: ConstraintLayout? = null
    var menu2: ConstraintLayout? = null
    var menu3: ConstraintLayout? = null
    private lateinit var btnRefresh : ImageButton
    private lateinit var pbHome : ProgressBar

    private lateinit var txtSaldo : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        txtSaldo = findViewById(R.id.txtSaldoHome)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        menu1 = findViewById(R.id.conMenu1)
        menu2 = findViewById(R.id.conMenu2)
        menu3 = findViewById(R.id.conMenu3)
        btnRefresh = findViewById(R.id.btnRefreshHome)
        pbHome = findViewById(R.id.pbHome)


        btnHistory.setOnClickListener(this)
        menu1?.setOnClickListener(this)
        menu2?.setOnClickListener(this)
        menu3?.setOnClickListener(this)
        btnRefresh?.setOnClickListener(this)
        pbHome.setOnClickListener(this)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
        txtSaldo.setText(toRupiah(getSaldoUser(this)).replace("Rp.",""))

    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {

            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.conMenu1 -> startActivity(Intent(this, PinjamanActivity::class.java))
            R.id.conMenu2 -> startActivity(Intent(this, PulsaPascabayarActivity::class.java))
            R.id.conMenu3 -> startActivity(Intent(this, TokenListrikActivity::class.java))
            R.id.btnRefreshHome->refrshSaldo()
            R.id.btnHistory -> startActivity(Intent(this, MutasiActivity::class.java))

        }
    }

    private fun refrshSaldo() {
        pbHome.visibility=View.VISIBLE
        val jsonParam = JSONObject()
        try {
            jsonParam.put("notelp", getMsisdn(this))
        }catch (e:Exception){
            println("asd exception create param [MainActiity] "+e.message)
            showListenerDialogMessage(this,"Oops, something wrong with our system",object:DialogListener{
                override fun onPositiveDialogBtnClickListener() { return }
                override fun onNegativeDialogBtnClickListener() {}
            })
        }

        val apiInterface = RetrofitServiceKotlin.getRetrofit().create(ApiServiceKotlin::class.java)
        val call  = apiInterface.getSaldo(jsonParam.toString())
        call.enqueue(object: Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                Toast.makeText(this@MainActivity, DESC_CANNOT_CONNECT,Toast.LENGTH_SHORT).show()
                pbHome.visibility = View.GONE
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
              try{
                  val data = JSONObject(response.body())
                  val rc = data.getString("rc")
                  when(rc){
                      "00"->{
                          txtSaldo.text = data.getJSONObject("data").getString("saldo")
                          setSaldoUser(this@MainActivity,txtSaldo.text.toString())
                      }
                      else -> Toast.makeText(this@MainActivity, DESC_SERVER_RESPONSE_FAILED,Toast.LENGTH_SHORT).show()
                  }
                  pbHome.visibility=View.GONE
              }catch (e:Exception){
                  Toast.makeText(this@MainActivity, DESC_SERVER_RESPONSE_FAILED,Toast.LENGTH_SHORT).show()
                  pbHome.visibility = View.GONE
              }
            }
        })



    }
}
