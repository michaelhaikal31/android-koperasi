package com.dak.koperasi.Interfaces


import retrofit2.Call
import retrofit2.http.*

interface  ApiServiceKotlin{

    @Headers("Content-Type: application/json")
    @POST("login")
    fun login(@Body body: String): Call<String>

    @Headers("Content-Type: application/json")
    @POST("getSaldo")
    fun getSaldo(@Body body: String): Call<String>

    @Headers("Content-Type: application/json")
    @POST("allProduct")
    fun getLayanan(@Body body: String): Call<String>

    @Headers("Content-Type: application/json")
    @POST("getAllKeranjang")
    fun getAllKeranjang(@Body body: String): Call<String>

}